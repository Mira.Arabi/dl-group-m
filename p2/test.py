# External
from numpy.core.shape_base import _arrays_for_stack_dispatcher
import torch
import math
import matplotlib.pyplot as plt
from statistics import *

# Internal
import modules as n
from optim import *
from train import *
from net import Network
from helper import *

# autograd globally off
torch.set_grad_enabled(False)

# center and radius of the disc 
center = (0.5, 0.5)
radius = 1/math.sqrt(2*math.pi)

# Dictionary of networks to test
networks = {
                1: [
                    n.Linear(2, 25), 
                    n.ReLU(),
                    n.Linear(25, 25),
                    n.ReLU(),
                    n.Linear(25, 25),
                    n.ReLU(),
                    n.Linear(25, 1),
                    n.Sigmoid()
                    ],
                2: [
                    n.Linear(2, 25), 
                    n.leakyReLU(),
                    n.Linear(25, 25),
                    n.leakyReLU(),
                    n.Linear(25, 25),
                    n.leakyReLU(),
                    n.Linear(25, 1),
                    n.Sigmoid()
                    ],   
                3: [
                    n.Linear(2, 25), 
                    n.Tanh(),
                    n.Linear(25, 25),
                    n.Tanh(),
                    n.Linear(25, 25),
                    n.Tanh(),
                    n.Linear(25, 1),
                    n.Sigmoid()
                    ]
            }

# dictionary to save training statistics
results_train = {
                "N1_5": {"u":0,"std":0},
                "N1_25": {"u":0,"std":0},
                "N2_5": {"u":0,"std":0},
                "N2_25": {"u":0,"std":0},
                "N2_25_NoSGD": {"u":0,"std":0}
                 }

# dictionary to save testing statistics
results_test = {
                "N1_5": {"u":0,"std":0},
                "N1_25": {"u":0,"std":0},
                "N2_5": {"u":0,"std":0},
                "N2_25": {"u":0,"std":0},
                "N2_25_NoSGD": {"u":0,"std":0}
                 }

# lists for plotting                 
acc_train_N1_5_list, acc_train_N2_5_list, acc_train_N1_25_list, acc_train_N2_25_list, acc_train_N2_25_NoSGD_list = [], [], [], [], []
acc_test_N1_5_list, acc_test_N2_5_list, acc_test_N1_25_list, acc_test_N2_25_list, acc_test_N2_25_NoSGD_list = [], [], [], [], []


# network parameters
lr1 = 1e-4
lr2 = 5e-4
gamma = 0.9
mini_batch_size1 = 5
mini_batch_size2 = 25
nb_epochs1 = 100
nb_epochs2 = 250

# 190 runs with different dataset each time
for i in range(10):
    # Initialize data for every run
    train_input, train_target = generate_disc_set(1000, center, radius)
    test_input, test_target = generate_disc_set(1000, center, radius)
    
    # initialize a network from the networks dictionary
    N1 = Network(networks[1])
    N2 = Network(networks[2])

    # optimizers (SGD, with different parameters)
    optimizer1_5 = SGD(N1, mini_batch_size1, lr1, gamma)
    optimizer1_25 = SGD(N1, mini_batch_size2, lr2, gamma)
    optimizer2_5 = SGD(N2, mini_batch_size1, lr1, gamma)
    optimizer2_25 = SGD(N2, mini_batch_size2, lr2, gamma)

    ## N1 with mini_batch_size=5 & MSE, with SGD
    # Train
    loss_train_N1_5, acc_train_N1_5 = train_model(N1, train_input, train_target, n.MSE(), lr1, gamma, mini_batch_size1, nb_epochs1, optimizer=optimizer1_5)
    # Test
    acc_test_N1_5, output_N1_5 = test_model(N1, test_input, test_target, mini_batch_size1)
    # Save
    acc_train_N1_5_list.append(acc_train_N1_5[-1])
    acc_test_N1_5_list.append(acc_test_N1_5)
    # Print
    print(f' Network1 mb=5 train_acc = {acc_train_N1_5[-1]*100}')
    print(f' Network1 mb=5 test_acc = {acc_test_N1_5*100}')

    ## N1 with mini_batch_size=25 & MSE, with SGD
    # Train
    loss_train_N1_25, acc_train_N1_25 = train_model(N1, train_input, train_target, n.MSE(), lr2, gamma, mini_batch_size2, nb_epochs2, optimizer=optimizer1_25)
    # Test
    acc_test_N1_25, output_N1_25 = test_model(N1, test_input, test_target, mini_batch_size2)
    # Save
    acc_train_N1_25_list.append(acc_train_N1_25[-1])
    acc_test_N1_25_list.append(acc_test_N1_25)
    # Print
    print(f' Network1 mb=5 train_acc = {acc_train_N1_25[-1]*100}')
    print(f' Network1 mb=5 test_acc = {acc_test_N1_25*100}')

    ## N2 with mini_batch_size=5 & BCE, with SGD
    # Train
    loss_train_N2_5, acc_train_N2_5 = train_model(N2, train_input, train_target, n.BCE(), lr1, gamma, mini_batch_size1, nb_epochs1, optimizer=optimizer2_5)
    # Test
    acc_test_N2_5, output_N2_5 = test_model(N2, test_input, test_target, mini_batch_size1)
    # Save
    acc_train_N2_5_list.append(acc_train_N2_5[-1])
    acc_test_N2_5_list.append(acc_test_N2_5)
    # Print
    print(f' Network1 mb=5 train_acc = {acc_train_N2_5[-1]*100}')
    print(f' Network1 mb=5 test_acc = {acc_test_N2_5*100}')

    ## N2 with mini_batch_size=25 & BCE, with SGD
    # Train
    loss_train_N2_25, acc_train_N2_25 = train_model(N2, train_input, train_target, n.BCE(), lr1, gamma, mini_batch_size2, nb_epochs2, optimizer=optimizer2_25)
    # Test
    acc_test_N2_25, output_N2_25 = test_model(N2, test_input, test_target, mini_batch_size2)
    # Save
    acc_train_N2_25_list.append(acc_train_N2_25[-1])
    acc_test_N2_25_list.append(acc_test_N2_25)
    # Print
    print(f' Network1 mb=5 train_acc = {acc_train_N2_25[-1]*100}')
    print(f' Network1 mb=5 test_acc = {acc_test_N2_25*100}')

    ## N2 with mini_batch_size=25 & BCE, with no SGD
    # Train
    loss_train_N2_25_NoSGD, acc_train_N2_25_NoSGD = train_model(N2, train_input, train_target, n.BCE(), lr1, gamma, mini_batch_size2, nb_epochs2)
    # Test
    acc_test_N2_25_NoSGD, output_N2_25_NoSGD = test_model(N2, test_input, test_target, mini_batch_size2)
    # Save
    acc_train_N2_25_NoSGD_list.append(acc_train_N2_25_NoSGD[-1])
    acc_test_N2_25_NoSGD_list.append(acc_test_N2_25_NoSGD)
    # Print
    print(f' Network1 mb=5 train_acc = {acc_train_N2_25_NoSGD[-1]*100}')
    print(f' Network1 mb=5 test_acc = {acc_test_N2_25_NoSGD*100}')
    
    if i==0:
        # Plot test data and network output
        plot_data(test_input, test_target, center=center, radius=radius, title='Test input x Test target')
        plot_data(test_input, output_N2_25.int(), center=center, radius=radius, title='Test input x Network2 output')
        plot_data(test_input, output_N2_25_NoSGD.int(), center=center, radius=radius, title='Test input x Network2 output No SGD')

# dictionaries for plotting
d_plot1 = {
            'loss_train_N1_5':loss_train_N1_5,
            'loss_train_N2_5':loss_train_N2_5
        }
d_plot2 = {
            'loss_train_N1_25':loss_train_N1_25,
            'loss_train_N2_25':loss_train_N2_25
        }
d_plot3 = {
            'acc_train_N1_5':acc_train_N1_5,
            'acc_train_N2_5':acc_train_N2_5
        }
d_plot4 = {
            'acc_train_N1_25':acc_train_N1_25,
            'acc_train_N2_25':acc_train_N2_25
        }
d_plot5 = {
            'loss_train_N2_SGD':loss_train_N2_25,
            'loss_train_N2_25_NoSGD':loss_train_N2_25_NoSGD
        }

# Plot loss and accuracy 
plot_figures(d_plot1, f'Training Loss mini_batch_size=5 {i}')
plot_figures(d_plot2, f'Training Loss mini_batch_size=25 {i}')
plot_figures(d_plot3, f'Training Accuracy mini_batch_size=5 {i}', acc=True)
plot_figures(d_plot4, f'Training Accuracy mini_batch_size=25 {i}', acc=True)
plot_figures(d_plot5, f'Training Loss with and without SGD {i}')


# save results (mean and standard deviation)
results_train["N1_5"]["u"], results_train["N1_5"]["std"] = round(100*mean(acc_train_N1_5_list), 2), round(100*stdev(acc_train_N1_5_list), 2)
results_test["N1_5"]["u"], results_test["N1_5"]["std"] = round(100*mean(acc_test_N1_5_list), 2), round(100*stdev(acc_test_N1_5_list), 2)

results_train["N1_25"]["u"], results_train["N1_25"]["std"] = round(100*mean(acc_train_N1_25_list), 2), round(100*stdev(acc_train_N1_25_list), 2)
results_test["N1_25"]["u"], results_test["N1_25"]["std"] = round(100*mean(acc_test_N1_25_list), 2), round(100*stdev(acc_test_N1_25_list), 2)

results_train["N2_5"]["u"], results_train["N2_5"]["std"] = round(100*mean(acc_train_N2_5_list), 2), round(100*stdev(acc_train_N2_5_list), 2)
results_test["N2_5"]["u"], results_test["N2_5"]["std"] = round(100*mean(acc_test_N2_5_list), 2), round(100*stdev(acc_test_N2_5_list), 2)

results_train["N2_25"]["u"], results_train["N2_25"]["std"] = round(100*mean(acc_train_N2_25_list), 2), round(100*stdev(acc_train_N2_25_list), 2)
results_test["N2_25"]["u"], results_test["N2_25"]["std"] = round(100*mean(acc_test_N2_25_list), 2), round(100*stdev(acc_test_N2_25_list), 2)

results_train["N2_25_NoSGD"]["u"], results_train["N2_25_NoSGD"]["std"] = round(100*mean(acc_train_N2_25_NoSGD_list), 2), round(100*stdev(acc_train_N2_25_NoSGD_list), 2)
results_test["N2_25_NoSGD"]["u"], results_test["N2_25_NoSGD"]["std"] = round(100*mean(acc_test_N2_25_NoSGD_list), 2), round(100*stdev(acc_test_N2_25_NoSGD_list), 2)

print(f'Training Results: {results_train}')
print(f'Testing Results: {results_test}')



