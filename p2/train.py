# Internal
import torch


def train_model(model, train_input, train_target, criterion, lr, gamma, mini_batch_size, nb_epochs, optimizer=None):
    if type(model.mods[-1]).__name__ == 'Sigmoid':
        threshold = 0.5
    elif type(model.mods[-1]).__name__ == 'Tanh':
        threshold = 0
        
    loss_list = []
    acc_list = []
    for e in range(nb_epochs):
        acc_loss = 0
        nb_errors = 0

        for b in range(0, train_input.size(0), mini_batch_size):
            ## forward
            output = model.forward(train_input.narrow(0, b, mini_batch_size)) 
            y = train_target.narrow(0, b, mini_batch_size)    

            ## loss
            loss = criterion.forward(output.squeeze(), y)
            acc_loss = acc_loss + loss.item()
            
            ## zero grad
            if optimizer is not None:
                optimizer.zero_grad()
            else:
                model.zero_grad()
            
            ## gradient of loss
            g_loss = criterion.backward(output.squeeze(), y)

            ## backward
            model.backward(g_loss)
            
            ## gradient step
            if optimizer is not None:
                optimizer.step()
            else: 
                model.update_params(lr)
            
            output = (output>threshold).long()
            # if(e==(nb_epochs-1)):
            for k in range(mini_batch_size):
                if y[k] != output[k]:
                    nb_errors = nb_errors + 1
        
        accuracy_e = 1 - nb_errors/train_input.size(0)
        print(f'{e}, acc: {accuracy_e}')
        print(e, acc_loss/train_input.size(0))
        loss_list.append(acc_loss/train_input.size(0))
        acc_list.append(accuracy_e)

    return loss_list, acc_list #last of acc_list is the final accuracy

def test_model(model, test_input, test_target, mini_batch_size):
    all_output = torch.zeros((1000, 1))
    if type(model.mods[-1]).__name__ == 'Sigmoid':
        threshold = 0.5
    elif type(model.mods[-1]).__name__ == 'Tanh':
        threshold = 0

    nb_errors = 0
    for b in range(0, test_input.size(0), mini_batch_size):
        output = model.forward(test_input.narrow(0, b, mini_batch_size)) 
        output = (output>threshold).long()
        y = test_target.narrow(0, b, mini_batch_size)

        for k in range(mini_batch_size):
            if y[k] != output[k]:
                nb_errors = nb_errors + 1
        all_output[b: b+mini_batch_size] = output
    accuracy_e = 1 - nb_errors/test_input.size(0)
    return accuracy_e, all_output.squeeze()