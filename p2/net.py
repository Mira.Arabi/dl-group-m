# Internal
import modules as n

## Generic class to build a network with the list of modules          
class Network():

    def __init__(self, mods) -> None:
        self.mods = mods

    # forward
    def forward(self, train_input):
        x = n.Sequential(self.mods).forward(train_input)
        return x

    # backward
    def backward(self, g_loss):
        y = n.Sequential(self.mods).backward(g_loss)
        return y

    #get params
    def param(self):
        return n.Sequential(self.mods).param()
    
    # update params
    def update_params(self, lr):
        n.Sequential(self.mods).update_params(lr)
    
    # zero grad
    def zero_grad(self):
        n.Sequential(self.mods).zero_grad()
