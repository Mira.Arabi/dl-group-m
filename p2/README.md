# Deep Learning Mini Project 2 Group M - Fall 2021

This GitLab repository folder *p2* contains the necessary files to complete the Project 2. 

The file **test.py** generates the data, creates the neural network models, trains and tests the models.

The pdf file contains the report which explains the problem and the implementation in order to solve it.
