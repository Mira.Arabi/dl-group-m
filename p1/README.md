# Deep Learning Mini Project 1 Group M - Fall 2021

This GitLab repository folder *p1* contains the necessary files to complete the Project 1. 

The file **test.py** generates the data, creates the neural network models, trains and tests the models.

The pdf file contains the report which explains the problem and the implementation in order to solve it.
