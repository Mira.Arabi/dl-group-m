from os import error
import torch
from torch import nn
from torch.nn import functional as F
from torch.nn.modules.activation import ReLU, Sigmoid, Softmax
from torch.nn.modules.batchnorm import BatchNorm2d
from torch.nn.modules.conv import Conv2d
from torch.nn.modules.dropout import Dropout
from torch.nn.modules.pooling import MaxPool2d

## Basic MLP Network with 2 hidden layers
class NN(nn.Module):
    def __init__(self, nb_hidden):
        super().__init__()
        self.classifier = nn.Sequential(
            nn.Linear(392, nb_hidden, bias=True),
            nn.ReLU(inplace=True),
            nn.BatchNorm1d(nb_hidden),
            nn.Linear(nb_hidden, nb_hidden, bias=True),
            nn.ReLU(inplace=True),
            nn.BatchNorm1d(nb_hidden),
            nn.Linear(nb_hidden, 1, bias=True),
            Sigmoid()
        )

    def forward(self, x):
        x = x.view(-1, 392)
        x = self.classifier(x)
        return x

## VGG Convolutional Network with 2 hidden layers
class CNN_VGG(nn.Module):
    def __init__(self, nb_hidden):
        super().__init__()
        self.features = nn.Sequential(
            nn.Conv2d(2, 32, kernel_size=3),
            nn.ReLU(inplace=True),
            nn.Conv2d(32, 32, kernel_size=3),
            nn.ReLU(inplace=True),
            nn.BatchNorm2d(32),
            nn.MaxPool2d(kernel_size=2, stride=1),
            nn.Conv2d(32, 64, kernel_size=3),
            nn.ReLU(inplace=True),
            nn.Conv2d(64, 64, kernel_size=3),
            nn.ReLU(inplace=True),
            nn.BatchNorm2d(64),
            nn.MaxPool2d(kernel_size=2, stride=2, padding=1),
        )

        self.classifier = nn.Sequential(
            nn.Linear(576, nb_hidden),
            nn.ReLU(inplace=True),
            nn.Dropout(p=0.5),
            nn.Linear(nb_hidden, nb_hidden),
            nn.ReLU(inplace=True),
            nn.Dropout(p=0.5),
            nn.Linear(nb_hidden, 1),
            Sigmoid()
        )

    def forward(self, x):
        x = self.features(x)
        x = x.view(-1, 576)
        x = self.classifier(x)
        return x

#Problem 1 Part 2:
## 2 Networks: Classifier + Comparer (Weight Sharing)
## MLP Network with 2 hidden layers for classification only
class NN_Classification(nn.Module):
    def __init__(self, nb_hidden):
        super().__init__()
        self.classifier = nn.Sequential(
            nn.Linear(196, nb_hidden),
            nn.ReLU(inplace=True),
            nn.BatchNorm1d(nb_hidden),
            nn.Linear(nb_hidden, nb_hidden),
            nn.ReLU(inplace=True),
            nn.BatchNorm1d(nb_hidden),
            nn.Linear(nb_hidden, 10),
            Sigmoid()
        )

    def forward(self, x1, x2):
        x1 = self.classifier(x1.view(-1, 196))
        x2 = self.classifier(x2.view(-1, 196))
        return torch.cat((x1, x2), 1)

## Convolutional Network with 2 hidden layers for classification only
class CNN_Classification(nn.Module):
    def __init__(self, nb_hidden):
        super().__init__()
        self.features = nn.Sequential(
            nn.Conv2d(1, 32, kernel_size=3),
            nn.ReLU(inplace=True),
            nn.Conv2d(32, 32, kernel_size=3),
            nn.ReLU(inplace=True),
            nn.BatchNorm2d(32),
            nn.MaxPool2d(kernel_size=2, stride=1),
            nn.Conv2d(32, 64, kernel_size=3),
            nn.ReLU(inplace=True),
            nn.Conv2d(64, 64, kernel_size=3),
            nn.ReLU(inplace=True),
            nn.BatchNorm2d(64),
            nn.MaxPool2d(kernel_size=2, stride=2, padding=1),
        )

        self.classifier = nn.Sequential(
            nn.Linear(576, nb_hidden),
            nn.ReLU(inplace=True),
            nn.BatchNorm1d(nb_hidden),
            nn.Dropout(p=0.5),
            nn.Linear(nb_hidden, nb_hidden),
            nn.ReLU(inplace=True),
            nn.BatchNorm1d(nb_hidden),
            nn.Dropout(p=0.5),
            nn.Linear(nb_hidden,10),
            Softmax(dim=0)
        )

    def forward(self, x1, x2):
        x1 = self.features(x1.unsqueeze(1))
        x2 = self.features(x2.unsqueeze(1))
        x1 = x1.view(-1, 576)
        x2 = x2.view(-1, 576)
        x1 = self.classifier(x1)
        x2 = self.classifier(x2)
        # concatenate both outputs into one tensor of size 20
        return torch.cat((x1, x2), 1)

## MLP Network with 2 hidden layers for comparison only
class MLP_Comparer(nn.Module):
    def __init__(self, nb_hidden):
        super().__init__()

        self.classifier = nn.Sequential(
            # input is concatenation of classification output: 
            # tensor of 20 elements (10 classes for each input)
            nn.Linear(20, nb_hidden),
            nn.ReLU(inplace=True),
            nn.BatchNorm1d(nb_hidden),
            nn.Linear(nb_hidden, nb_hidden),
            nn.ReLU(inplace=True),
            nn.BatchNorm1d(nb_hidden),
            nn.Linear(nb_hidden, 1),
            Sigmoid()
        )

    def forward(self, x):
        x = self.classifier(x)
        return x

## MLP Siamese Network with classification and comparison combined
class NN_WS(nn.Module):
    def __init__(self, nb_hidden):
        super().__init__()
        self.classifier = NN_Classification(nb_hidden)
        self.comparer = MLP_Comparer(nb_hidden)

    def forward(self, x):
        x = self.comparer(self.classifier(x[:, 0], x[:,1]))
        return x

## Convolutional Siamese Network with classification and comparison combined
class CNN_WS(nn.Module):
    def __init__(self, nb_hidden):
        super().__init__()
        self.classifier = CNN_Classification(nb_hidden)
        self.comparer = MLP_Comparer(nb_hidden)

    def forward(self, x):
        x = self.comparer(self.classifier(x[:, 0], x[:,1]))
        return x

## MLP Siamese Network with classification and comparison combined + Auxiliary Loss
class NN_WS_AL(nn.Module):
    def __init__(self, nb_hidden):
        super().__init__()
        self.classifier = NN_Classification(nb_hidden)
        self.comparer = MLP_Comparer(nb_hidden)

    def forward(self, x):
        # output of classifier
        out = self.classifier(x[:,0], x[:,1])
        # output of comparer (final result)
        res = self.comparer(out)
        # keep both, for Auxiliary Loss
        return out, res

## Convolutional Siamese Network with classification and comparison combined + Auxiliary Loss
class CNN_WS_AL(nn.Module):
    def __init__(self, nb_hidden):
        super().__init__()
        self.classifier = CNN_Classification(nb_hidden)
        self.comparer = MLP_Comparer(nb_hidden)

    def forward(self, x):
        # output of classifier
        out = self.classifier(x[:,0], x[:,1])
        # output of comparer (final result)
        res = self.comparer(out)
        # keep both, for Auxiliary Loss
        return out, res