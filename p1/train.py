from net import *

## Train Model, works for both Basic + Weight Sharing Networks 
def train_model(model, train_input, train_target, lr, criterion, mini_batch_size, nb_epochs):
    loss_list = []
    acc_list = []
    eta = 1e-2
    
    for e in range(nb_epochs):
        acc_loss = 0
        nb_errors = 0

        for b in range(0, train_input.size(0), mini_batch_size):
            output = model(train_input.narrow(0, b, mini_batch_size))
            y = train_target.narrow(0, b, mini_batch_size)
            loss = criterion(output.view(-1), y.float())
            acc_loss = acc_loss + loss.item()

            optimizer = torch.optim.Adam(model.parameters(), lr, weight_decay=0)
            optimizer.zero_grad()
            loss.backward()
            optimizer.step()

            with torch.no_grad():
                for p in model.parameters():
                    p -= eta * p.grad
            
            output_b = (output.view(-1)>0.5).float()
            for k in range(mini_batch_size):
                if y[k] != output_b[k]:
                    nb_errors = nb_errors + 1

        # print(e, acc_loss)
        loss_list.append(acc_loss)
        accuracy_e = 1 - nb_errors/train_input.size(0)
        # print(f'{e}, acc: {accuracy_e}')
        acc_list.append(accuracy_e)

    return loss_list, acc_list

## Test Model, works for both Basic + Weight Sharing Networks 
def test_model(model, test_input, test_target, mini_batch_size):
    nb_errors = 0
    for b in range(0, test_input.size(0), mini_batch_size):
        output = model(test_input.narrow(0, b, mini_batch_size))
        output_b = (output.view(-1)>0.5).float()
        y = test_target.narrow(0, b, mini_batch_size)
        for k in range(mini_batch_size):
            if y[k] != output_b[k]:
                nb_errors = nb_errors + 1
    accuracy = 1 - nb_errors/test_input.size(0)
    return accuracy

## Train Model WSAL, works for Weight Sharing with Auxiliary Loss
def train_model_WSAL(model, train_input, train_target, train_classes, lr, criterion_class, criterion_comp, mini_batch_size, nb_epochs):
    loss_list = []
    acc_list = []
    eta = 1e-1
    for e in range(nb_epochs):
        acc_loss = 0
        nb_errors = 0

        for b in range(0, train_input.size(0), mini_batch_size):
            # ouput of classification, res from comparison
            out, res = model(train_input.narrow(0, b, mini_batch_size))
            # classes for images 1
            y1 = train_classes[:,0].narrow(0, b, mini_batch_size)
            # classes for images 2
            y2 = train_classes[:,1].narrow(0, b, mini_batch_size)
            # classification loss for images 1
            loss1 = criterion_class(out[:,0:10], y1)
            # classification loss for images 1
            loss2 = criterion_class(out[:,10:20], y2)
            y = train_target.narrow(0, b, mini_batch_size)
            # comparison loss
            loss3 = criterion_comp(res.view(-1), y.float())
            loss = 0.05*loss1 + 0.05*loss2 + 0.1*loss3
            acc_loss = acc_loss + loss.item()

            optimizer = torch.optim.Adam(model.parameters(), lr, weight_decay=1e-5)
            optimizer.zero_grad()
            loss.backward()
            optimizer.step()

            with torch.no_grad():
                for p in model.parameters():
                    p -= eta * p.grad

            output_b = (res.view(-1)>0.5).float()
            for k in range(mini_batch_size):
                if y[k] != output_b[k]:
                    nb_errors = nb_errors + 1

        # print(e, acc_loss)
        loss_list.append(acc_loss)
        accuracy_e = 1 - nb_errors/train_input.size(0)
        # print(f'{e}, acc: {accuracy_e}')
        acc_list.append(accuracy_e)

    return loss_list, acc_list

## Test Model WSAL, works for Weight Sharing with Auxiliary Loss
def test_model_WSAL(model, test_input, test_target, mini_batch_size):
    nb_errors = 0

    for b in range(0, test_input.size(0), mini_batch_size):
        _, output = model(test_input.narrow(0, b, mini_batch_size))
        output_b = (output.view(-1)>0.5).float()
        y = test_target.narrow(0, b, mini_batch_size)
        for k in range(mini_batch_size):
            if y[k] != output_b[k]:
                nb_errors = nb_errors + 1
    accuracy_e = 1 - nb_errors/test_input.size(0)
    return accuracy_e
