import matplotlib.pyplot as plt

# Plot function for plotting train accuracies, 
# and train losses for the NN and CNN of each architecture

def plot_figures(d, title, acc=False):
    for value in d.values():
        plt.plot(value)

    plt.xlabel("Epochs")
    if acc == False:
        plt.ylabel("Accuracy")
    else:
        plt.ylabel("Loss")

    plt.legend([k for k in d.keys()])
    plt.title(title)
    plt.savefig(f'{title}.png')
    plt.show()
